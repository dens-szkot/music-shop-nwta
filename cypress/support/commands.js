Cypress.Commands.add("login", () => {
  cy.visit("/signin");
  cy.get("input[placeholder = 'email@domain.com']").type("test@test.pl");
  cy.get("input[placeholder = 'password']").type("test123");
  cy.contains("button", "login").click();
  cy.wait(5000);
});
