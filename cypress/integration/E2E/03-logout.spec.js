/// <reference types="cypress" />

describe("Logout", () => {
  it("Logout", () => {
    cy.login();
    cy.contains("button", "account").click();
    cy.get("li[tabindex = '6']").click();
    cy.contains("button", "login").should("be.visible");
    cy.wait(10000);
  });
});
