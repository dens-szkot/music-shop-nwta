/// <reference types="cypress" />

describe("Order", () => {
  it("Order", () => {
    cy.login();
    cy.get(".gNjBCM").first().click();
    cy.get("button[tabindex = '2']").click();
    cy.get(".iuuBtu");
    cy.contains("button", "Order now").click();
    cy.contains("h2", "Order information").should("be.visible");
    cy.wait(10000);
  });
});
