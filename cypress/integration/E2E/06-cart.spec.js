/// <reference types="cypress" />

describe("Cart", () => {
  beforeEach(() => {
    cy.login();
  });
  it("Adding to cart", () => {
    cy.get(".gNjBCM").first().click();
    cy.get("button[tabindex = '2']").click();
    cy.get(".iuuBtu");
    cy.wait(10000);
  });
  it("Removing from cart", () => {
    cy.get(".hnVfoT").first().click();
    cy.get("button[tabindex = '2']").click();
    cy.contains("span", "Your cart is empty");
    cy.wait(10000);
  });
});
