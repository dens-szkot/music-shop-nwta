/// <reference types="cypress" />

describe("Changing account data", () => {
  it("Changeing account data", () => {
    cy.login();
    cy.contains("button", "account").click();
    cy.contains("li", "Settings").click();
    cy.get("input[name ='firstName']").clear().type("testName");
    cy.get("input[name = 'lastName']").clear().type("testLastName");
    cy.get("input[name ='phoneNumber']").clear().type("134235111");
    cy.contains("button", "save").click();
    cy.get("div[role = 'alert']");
    cy.wait(10000);
  });
});
