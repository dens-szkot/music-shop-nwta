/// <reference types="cypress" />

describe("SignUp", () => {
  it("Singup", () => {
    cy.visit("/signin");
    cy.contains("a", "Sign up").click();
    cy.get("input[placeholder = 'first name']").type("test");
    cy.get("input[placeholder = 'last name']").type("test");
    cy.get("input[placeholder = 'phone number']").type("123123123");
    cy.get("input[placeholder = 'email@domain.com']").type("test@test.pl");
    cy.get("input[placeholder = 'password']").type("test123");
    cy.contains("button", "register").click();
    cy.wait(10000);
  });
});
