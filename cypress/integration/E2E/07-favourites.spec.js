/// <reference types="cypress" />

describe("Adding and removing from favourites", () => {
  beforeEach(() => {
    cy.login();
  });
  it("Adding to favourites", () => {
    cy.contains("span", "Add to favourites").first().click();
    cy.wait(10000);
  });
  it("Removing from favourites", () => {
    cy.contains("span", "Remove from favourites").first().click();
    cy.wait(10000);
  });
});
