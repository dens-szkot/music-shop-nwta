/// <reference types="cypress" />

describe("Searching", () => {
  it("Searching", () => {
    cy.login();
    cy.get("input[placeholder = 'Search']").type("The Flamingos").type("{enter}");
    cy.contains("h3", "The Flamingos : Zoo in the Bathroom").should("be.visible");
    cy.wait(10000);
  });
});
